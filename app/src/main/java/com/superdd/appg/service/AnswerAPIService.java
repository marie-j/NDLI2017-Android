package com.superdd.appg.service;

import com.superdd.appg.model.Answer;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AnswerAPIService {

    @POST("/api/answers")
    Call<Answer> sendAnswer(@Body Answer answer);
}
