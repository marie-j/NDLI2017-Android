package com.superdd.appg.service;

import com.superdd.appg.model.Alert;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AlertAPIService {

    @POST("/api/alerts")
    Call<Alert> sendAlert(@Body Alert alert);
}
