package com.superdd.appg.service;

import com.superdd.appg.model.Question;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface QuestionAPIService {

    @GET("/api/questions")
    Call<List<Question>> getQuestions();
}
