package com.superdd.appg.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.superdd.appg.R;
import com.superdd.appg.RetrofitClient;
import com.superdd.appg.model.Alert;
import com.superdd.appg.service.AlertAPIService;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, OnLocationUpdatedListener, Callback<Alert> {

    private GoogleMap mMap;
    private LatLng mLatLng;
    private String mPhoneNumber;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions();
    }

    private void showProgressDialog(String title, String message) {

        if (this.mProgressDialog == null)
            this.mProgressDialog = new ProgressDialog(this);

        this.mProgressDialog.setTitle(title);
        this.mProgressDialog.setMessage(message);
        this.mProgressDialog.show();
    }

    private void dismissProgressDialog() {

        if (this.mProgressDialog != null && this.mProgressDialog.isShowing())
            this.mProgressDialog.dismiss();
    }

    public void getInfos() {
        getLocation();
        getPhoneNumber();
    }

    public void getLocation() {

        this.showProgressDialog(getString(R.string.position_gps), getString(R.string.get_gps_position));

        SmartLocation.with(this).location().oneFix().start(this);
    }

    @SuppressLint("MissingPermission")
    public void getPhoneNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        mPhoneNumber = telephonyManager.getLine1Number();
    }

    @Override
    public void onLocationUpdated(Location location) {
        this.dismissProgressDialog();

        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        initializeMap();
    }

    public void initializeMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public boolean isGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissions() {

        boolean permissionsGranted;

        permissionsGranted = isGranted(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionsGranted &= isGranted(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsGranted &= isGranted(Manifest.permission.READ_PHONE_STATE);
        permissionsGranted &= isGranted(Manifest.permission.INTERNET);

        if (!permissionsGranted) {

            String[] permissions = new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.INTERNET};

            ActivityCompat.requestPermissions(this, permissions, 200);
        } else {

            getInfos();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        boolean permissionsGranted;

        permissionsGranted = grantResults.length == 4;
        permissionsGranted &= grantResults[0] == PackageManager.PERMISSION_GRANTED;
        permissionsGranted &= grantResults[1] == PackageManager.PERMISSION_GRANTED;
        permissionsGranted &= grantResults[2] == PackageManager.PERMISSION_GRANTED;
        permissionsGranted &= grantResults[3] == PackageManager.PERMISSION_GRANTED;

        if (permissionsGranted)
            getInfos();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        String title = getString(R.string.my_location);
        MarkerOptions options = new MarkerOptions().position(mLatLng).title(title);
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 13));
    }

    public void sendAlert(View view) {
        String url = getString(R.string.api_url);
        AlertAPIService service = RetrofitClient.getClient(url).create(AlertAPIService.class);
        Alert alert = new Alert(mPhoneNumber, mLatLng.latitude, mLatLng.longitude);
        service.sendAlert(alert).enqueue(this);
        this.showProgressDialog("Envoi", "Envoi des informations à l'opérateur");
    }

    @Override
    public void onResponse(Call<Alert> call, Response<Alert> response) {
        this.dismissProgressDialog();

        if (response.code() == 200) {
            Intent intent = new Intent(MainActivity.this, QuestionActivity.class);
            intent.putExtra("phone", mPhoneNumber);
            startActivity(intent);
        } else
            onError();
    }

    @Override
    public void onFailure(Call<Alert> call, Throwable t) {
        this.dismissProgressDialog();

        onError();
    }

    public void onError() {
        Intent intent = new Intent(this, PhoneNumberActivity.class);
        startActivity(intent);
    }
}
