package com.superdd.appg.activity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;

import com.superdd.appg.R;

import java.util.Locale;

public class CallActivity extends AppCompatActivity {

    TextToSpeech mTTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        initTTS();
    }

    public void initTTS() {
        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR)
                    mTTS.setLanguage(Locale.FRANCE);
                call();
            }
        });
    }

    public void call() {
        String simulation = "Vous avez demandé la police ne quittez pas";
        for (int i = 0; i < 3; i++) {
            mTTS.speak(simulation, TextToSpeech.QUEUE_ADD, null);
            mTTS.playSilence(1000, TextToSpeech.QUEUE_ADD, null);
        }
    }

}
