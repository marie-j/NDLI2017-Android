package com.superdd.appg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.superdd.appg.R;
import com.superdd.appg.RetrofitClient;
import com.superdd.appg.model.Answer;
import com.superdd.appg.model.Question;
import com.superdd.appg.service.AnswerAPIService;
import com.superdd.appg.service.QuestionAPIService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class QuestionActivity extends AppCompatActivity implements Callback<Answer> {

    Retrofit mRetrofit;
    AnswerAPIService mService;
    List<Question> mQuestions;
    int mCurrentQuestion;
    Answer mAnswer;
    TextToSpeech mTTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        initTTS();

        String url = getString(R.string.api_url);
        mRetrofit = RetrofitClient.getClient(url);
        mService = mRetrofit.create(AnswerAPIService.class);
        mCurrentQuestion = 0;
        mQuestions = new ArrayList<>();
        getQuestions();

        mAnswer = new Answer(getIntent().getStringExtra("phone"));


    }

    public void initTTS() {
        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR)
                    mTTS.setLanguage(Locale.FRANCE);
            }
        });
    }

    public void displayQuestion() {
        Question question = mQuestions.get(mCurrentQuestion);
        ((TextView) findViewById(R.id.question)).setText(question.getText());

        mTTS.speak(question.getText(), TextToSpeech.QUEUE_FLUSH, null);
    }

    public void answer(boolean answer) {
        mAnswer.setAnswer(answer);
        mAnswer.setQuestionId(mQuestions.get(mCurrentQuestion).getId());
        mService.sendAnswer(mAnswer).enqueue(this);

        if (mCurrentQuestion == mQuestions.size() - 1) {
            mTTS.shutdown();
            Intent intent = new Intent(this, CallActivity.class);
            startActivity(intent);
        } else {
            mCurrentQuestion++;
            displayQuestion();
        }
    }

    public void answerYes(View view) {
        answer(true);
    }

    public void answerNo(View view) {
        answer(false);
    }

    public void getQuestions() {
        mRetrofit.create(QuestionAPIService.class).getQuestions().enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                if (response.code() == 200) {
                    mQuestions = response.body();
                    displayQuestion();
                } else
                    onError();


            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                onError();
            }
        });
    }

    @Override
    public void onResponse(Call<Answer> call, Response<Answer> response) {
        if (response.code() == 200)
            displayQuestion();
        else
            onError();
    }

    @Override
    public void onFailure(Call<Answer> call, Throwable t) {
        onError();
    }

    public void onError() {
        Intent intent = new Intent(this, PhoneNumberActivity.class);
        startActivity(intent);
    }
}
